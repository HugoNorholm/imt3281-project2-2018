package no.ntnu.imt3281.ludo.server;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.rmi.server.ExportException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import no.ntnu.imt3281.ludo.logic.Ludo;
import no.ntnu.imt3281.ludo.logic.NoRoomForMorePlayersException;

/**
 * 
 * 
 * @author Hugo
 *
 */
public class Server{
	
	private final static ConcurrentHashMap<String, Client> clients = new ConcurrentHashMap<>();
	private final static ConcurrentHashMap<String, Chat> chats = new ConcurrentHashMap<>();
	private final static ArrayList<Game> gameq = new ArrayList<>();
	private final static ArrayList<Game> games = new ArrayList<>();
	
	static ExecutorService executor = Executors.newCachedThreadPool();
	
	private static int[] port = new int[] {9010,9020,9030,9040};
	
    private final static Logger logger = Logger.getLogger("Server");
    private static boolean shutdown = false;
	
	
	public static void main(String[] args) {
		
		String sqlCreateUserTable = "CREATE TABLE USERS("
                + "USERNAME VARCHAR(64) NOT NULL,"
                + "PASSWORD VARCHAR(255) NOT NULL,"
                + "GAMESPLAYED INT,"
                + "GAMESWON INT,"
                + "PRIMARY KEY (USERNAME))";
		
		try (Connection con = DriverManager.getConnection("jdbc:derby:LudoDB;create=true")) {
			Statement stmt = con.createStatement();
			stmt.execute(sqlCreateUserTable);

		} catch (SQLException sqle) {
			//sqle.printStackTrace();
		}
		

		
		
		addChat("MASTER");
		
		
		executor.execute(()->signupListenerThread());
		executor.execute(()->loginListenerThread());	
		executor.execute(()->chatJoinListenerThread());	
		executor.execute(()->gameListenerThread());	
	}
private static void gameListenerThread() {
	try (ServerSocket serverSocket = new ServerSocket(port[3])) {
		while (!shutdown) {		// Run until server stopping
			Socket s = null;
			try {
				s = serverSocket.accept();
			} catch (IOException e) {
				logger.log(Level.SEVERE, "Error while getting client connection: "+port[2], e);
				System.exit(0);
			}
			try {
				joinGame(s);	// Method that gets username and adds client to hashmap
			} catch (IOException e) {
				logger.log(Level.FINER, "Unable to establish connection with client", e);
			}
		}
	} catch (IOException e) {
		logger.log(Level.SEVERE, "Unable to listen to port: "+port[2], e);
		System.exit(0);
	}	
		
	}

private static void joinGame(Socket s) throws IOException {
	final Client client = new Client(s);
	if(gameq.isEmpty()) { // no games in q
		Game t = new Game();
		gameq.add(t);
	}
	Game t = gameq.get(0);
		try {
			t.ludo.addPlayer(client.getName());
			t.gameClients.put(client.getName(), client);
		} catch (NoRoomForMorePlayersException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
}



/**
 * Creates a new chat room
 * @param string name of chat room
 */
	private static void addChat(String string) {
		Chat temp = new Chat(string);
		chats.put(string, temp);
		
	}
	/**
	 * Listens for people wanting to join a chat
	 */
	private static void chatJoinListenerThread() {
		try (ServerSocket serverSocket = new ServerSocket(port[2])) {
			while (!shutdown) {		// Run until server stopping
				Socket s = null;
				try {
					s = serverSocket.accept();
				} catch (IOException e) {
					logger.log(Level.SEVERE, "Error while getting client connection: "+port[2], e);
					System.exit(0);
				}
				try {
					joinChat(s);	// Method that gets username and adds client to hashmap
				} catch (IOException e) {
					logger.log(Level.FINER, "Unable to establish connection with client", e);
				}
			}
		} catch (IOException e) {
			logger.log(Level.SEVERE, "Unable to listen to port: "+port[2], e);
			System.exit(0);
		}	
	}
	/**
	 * Join a chat
	 * @param s socket being added to chat
	 * @throws IOException connection broken early
	 */
	private static void joinChat(Socket s) throws IOException {
		final Client client = new Client(s);
		String chatname = client.read();
		System.out.println(chatname);
		Chat chat = chats.get(chatname);
		if (chat!= null) {
			chat.chatClients.put(client.getName(), client);
			chat.chatClients.forEachKey(100, name-> client.write("JOIN:"+name)); // Let the new client know the name of the existing clients
			chat.chatClients.put(client.getName(), client);
			chat.messages.add("JOIN:" +client.getName());
		} else {
			client.write("failure");
			client.close();
		}
		
	}
/**
 * Listens for clients logging in and adds them to client hashmap
 * approptiate respone otherwise
 */
	private static void loginListenerThread() {
		try (ServerSocket serverSocket = new ServerSocket(port[1])) {
			while (!shutdown) {		// Run until server stopping
				Socket s= null;
				try {
					s = serverSocket.accept();
				} catch (IOException e) {
					logger.log(Level.SEVERE, "Error while getting client connection: "+port[0], e);
					System.exit(0);
				}
				try {
					login(s);	// Method that gets username and adds client to hashmap
					
				} catch (IOException e) {
					logger.log(Level.FINER, "Unable to establish connection with client", e);
				}
			}
		} catch (IOException e) {
			logger.log(Level.SEVERE, "Unable to listen to port: "+port[0], e);
			System.exit(0);
		}

	}
	/**
	 * Log in user
	 * @param s socket of user being logged in
	 * @throws IOException connection broken
	 */
	private static void login(Socket s) throws IOException {
		final Client client = new Client(s);
		String user = client.name;
		String pass = client.input.readLine();
			
			//hash pass so it matches the pass in db
			
			String sqlLoginUser = "SELECT * FROM USERS WHERE USERNAME ='"+user+"'"; 
			
			try (Connection con = DriverManager.getConnection("jdbc:derby:LudoDB")) {
				Statement stmt = con.createStatement();
				ResultSet rs = stmt.executeQuery(sqlLoginUser);
		        rs.next();
		        System.out.println(rs.getString("USERNAME") + " : " + rs.getString("PASSWORD")+ " : " + rs.getString("GAMESPLAYED"));
		        if (rs == null){
		        	throw new Exception("Input can not be null");
				}

		        if (pass.equals(rs.getString("PASSWORD"))) {
					client.write("success");
					clients.put(user,client);
					executor.execute(()->clientListenerThread(client));
		        } else {
					client.write("wrong");
					client.close();
		        }
			} catch (SQLException sqle) {
				System.out.println(sqle);
				sqle.printStackTrace();
				client.write("failure");
				client.close();
			} catch (Exception e) {
				logger.log(Level.WARNING, e.toString(), e);
			}
			
		
		
	}
	/**
	 * Listens for client requests
	 * @param client being listened to
	 */

	private static void clientListenerThread(Client client) {
		while(client.active) {
			String request=client.read();
			if(request!=null) {
				
			String[] requests = request.split(":",2);
			System.out.println(requests[0]);
			if(requests[0].startsWith("C")) {
				System.out.println("C works");
				if (chats.containsKey(requests[1])) {
					System.out.println("cant create chat");
					client.write("exists");
				}
				else {
					addChat(requests[1]);
					System.out.println("create chat");
					client.write("success");
				}
			}
			if(requests[0].startsWith("RL")) {
				String roomlist = null;
				for(Entry<String, Chat> entry : chats.entrySet()) {
				    String key = entry.getKey();
				    roomlist = roomlist + key +":";
				}
				client.write(roomlist);
			}
			}
		}
	}
	/**
	 * Listens for new signuprequests
	 */
	private static void signupListenerThread() {
		try (ServerSocket serverSocket = new ServerSocket(port[0])) {
			while (!shutdown) {		// Run until server stopping
				Socket s= null;
				try {
					s = serverSocket.accept();
				} catch (IOException e) {
					logger.log(Level.SEVERE, "Error while getting client connection: "+port[0], e);
					System.exit(0);
				}
				try {
					signup(s);	// Method that gets username and adds client to hashmap
				} catch (IOException e) {
					logger.log(Level.FINER, "Unable to establish connection with client", e);
				}
			}
		} catch (IOException e) {
			logger.log(Level.SEVERE, "Unable to listen to port: "+port[0], e);
			System.exit(0);
		}
	}
	
	/**
	 * Create a new user
	 * @param s socket being used for comm
	 * @throws IOException connection broken
	 */
	private static void signup(Socket s) throws IOException {
		final Client client = new Client(s);
		String user = client.name;
		String pass = client.input.readLine();
		//hash password and maybe username
		
		String sqlCreateUser = "INSERT INTO USERS VALUES ("
                + "'"+user+"',"
                + "'"+pass+"',"
                + 0 +","
                + 0 +")";
		try (Connection con = DriverManager.getConnection("jdbc:derby:LudoDB")) {
			Statement stmt = con.createStatement();
			stmt.execute(sqlCreateUser);
			System.out.println("User: "+user+" created!");
			client.write("User: "+user+" created!");

		} catch (SQLException sqle) {
			System.out.println("User: "+user+" already exists");
			client.write("User: "+user+" already exists");			
		}
		client.close();
	}
	/**
	 * Client object mostly "borowed" from chatserver.java
	 * @author hugon
	 *
	 */
	static class Client {
    	private String name;
    	private Socket sock;
    	private BufferedReader input;
    	private BufferedWriter output;
    	private boolean active = true;
    	
    	/**
    	 * Gets a socket and creates reader/writer objects and reads the name of the client/user
    	 * 
    	 * @param s the socket from the serversocket
    	 * @throws IOException if unable to create reader/writer objects or read username.
    	 */
    	public Client (Socket s) throws IOException {
    		sock = s;
    		input = new BufferedReader(new InputStreamReader(s.getInputStream()));
    		output = new BufferedWriter(new OutputStreamWriter(s.getOutputStream()));
    		name = input.readLine();
    	}
    	
    	/**
    	 * read msg from client
    	 * @return msg from client
    	 */
    	
    	public String read() {
    		try {
				if (input.ready()) {
					return input.readLine();
				} else {
					return null;
				}
			} catch (IOException e) {
				active = false;	// If unable to read (IO error) mark as inactive, this will remove the client from the server
				return null;
			}
    	}
    	/**
    	 * Write to client
    	 * @param msg being sent
    	 */
    	public void write(String msg) {
    		try {
				output.write(msg);
				output.newLine();	// Must send newline for client to be able to read
				output.flush();		// Nothing is sent without flushing
			} catch (IOException e) {
				active = false; // If unable to write (IO error) mark as inactive, this will remove the client from the server
			}
    	}
    	
    	public String getName() {
    		return name;
    	}
    	/**
    	 * Close connection
    	 */
    	public void close() {
    		try {
    			active = false;
				input.close();
				output.close();
				sock.close();
			} catch (IOException e) {
				// This connection will be dropped anyway, nothing much to do about it
			}
    	
	}
	
}
	/**
	 * Class for a chat object in the server
	 * @author hugon
	 *
	 */
	
	static class Chat{
		
    	private final ConcurrentHashMap<String, Client> chatClients = new ConcurrentHashMap<>();
    	private LinkedList<String> messages = new LinkedList<String>(); 
    	private String chatName;
    	private boolean active = true;
    	
    	public Chat(String name) {
    		System.out.println(name);
    		chatName = name;
    		chats.put(name, this);
    		executor.execute(()->chatSenderThread(this));
    		executor.execute(()->chatListenerThread(this));	
    		
    	}
/**
 * Listen for incoming messages for all clients and stores them in the messages list
 * @param chat chat listening to
 */
		private void chatListenerThread(Chat chat) {
			while (chat.active) {
				
				chatClients.forEachValue(100, client-> {	
					String msg = client.read();		// Read message from client
					if (msg!=null&&msg.equals("§§BYEBYE§§")) {	// Client says goodbye

						if (clients.remove(client.getName())!=null) {
							clientRemoved(client);		// Let other clients know this client has left, adds message to textarea
						}
					} else if (msg!=null) {
						messages.add("MSG:"+client.getName()+">"+msg);	// Add message to message queue
					}
				});
				try {
					Thread.sleep(10);	// Prevent excessive processor usage
				} catch (InterruptedException e) {
					Thread.currentThread().interrupt();
				}
			}
		}
		/**
		 * Listens to a chat and sends incoming messages to all connected clients
		 * @param chat chat listening to
		 */
		private void chatSenderThread(Chat chat) {
			while (chat.active) {
				if(messages.size()>0){

					String s = messages.removeLast();

					chat.chatClients.forEachValue(100, client->client.write(s));	// Do in parallel if more than 100 clients
					// Clients that have IO error was marked inactive 
					chat.chatClients.forEachValue(100, client-> {	// Remove clients that could not receive message
						if (!client.active&&clients.remove(client.getName())!=null) {	// Client is inactive and still existed in the hashmap
							clientRemoved(client);
						}
					});
				}
				try {
					Thread.sleep(10);	// Prevent excessive processor usage
				} catch (InterruptedException e) {
					Thread.currentThread().interrupt();
				}
			}
			}
		/**
		 * Remove a client from the chat
		 * @param client being removed
		 */
		private void clientRemoved(Client client) {
			messages.add("DISCONNECTED:" +client.getName());	// Message will be sent to all other clients
		}
		
	}
	
	static class Game {
		Ludo ludo;
		private final ConcurrentHashMap<String, Client> gameClients = new ConcurrentHashMap<>();
    	private LinkedList<String> messages = new LinkedList<String>(); 
    //	private static Logger logger;
    //	private int gameID; // to be used for private games
    	private boolean active = true;
		
    	public Game() {
    		

    		
    		executor.execute(()->{
				try {
					gameQThread(this);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					System.out.println(e.getMessage());
				}
			});
    		executor.execute(()->gameSenderThread(this));
    		executor.execute(()->gameListenerThread(this));	
    		
    	}


		private void gameQThread(Game game2) throws InterruptedException {
			while(game2.ludo.status.equalsIgnoreCase("Created")) {
				TimeUnit.SECONDS.sleep(30);
			}
			if(gameq.contains(game2)) {
			gameq.remove(game2);
			games.add(game2);			
			}
		}

		private Object gameListenerThread(Game game2) {
			// TODO Auto-generated method stub
			return null;
		}

		private void gameSenderThread(Game game2) {
			while (game2.active) {
				if(messages.size()>0){

					String s = messages.removeLast();

					game2.gameClients.forEachValue(100, client->client.write(s));	// Do in parallel if more than 100 clients
					// Clients that have IO error was marked inactive 
					game2.gameClients.forEachValue(100, client-> {	// Remove clients that could not receive message
						if (!client.active&&clients.remove(client.getName())!=null) {	// Client is inactive and still existed in the hashmap
						
							game2.ludo.removePlayer(client.getName());
						}
					});
				}
				try {
					Thread.sleep(10);	// Prevent excessive processor usage
				} catch (InterruptedException e) {
					Thread.currentThread().interrupt();
				}
			}
		}



		
	}
	
}