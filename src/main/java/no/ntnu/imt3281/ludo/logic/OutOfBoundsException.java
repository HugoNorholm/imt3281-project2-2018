package no.ntnu.imt3281.ludo.logic;

/**
 * Used when picked up mouse click out of bounds.
 */
public class OutOfBoundsException extends Exception {
    private String message;

    public OutOfBoundsException (String _string) {
        message = _string;
    }

    /**
     *
     */
    private static final long serialVersionUID = 1L;

}
