package no.ntnu.imt3281.ludo.logic;

public class PlayerEvent {

	public static final int WAITING = 0;
	public static final int PLAYING = 1;
	public static final int LEFTGAME = 2;
	public static final int WON = 3;
	
	private Ludo game;
	private int player;
	private int event;
	
	public PlayerEvent(Ludo ludo, int i, int j) {
		game = ludo;
		player= i;
		event = j;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
	            return true;
	        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }
        PlayerEvent temp = (PlayerEvent) obj;
		if(temp.game != this.game) {
			return false;
		}
		if(temp.player != this.player) {
			return false;
		}
		if(temp.event != this.event) {
			return false;
		}

		return true;
	}

	/**
	 * Override of library func to avoid deep conflicts.
	 * @return 0
	 */
	@Override
	public int hashCode() {
		return 0;
	}
}
