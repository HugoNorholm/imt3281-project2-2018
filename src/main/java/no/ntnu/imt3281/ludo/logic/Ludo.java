package no.ntnu.imt3281.ludo.logic;

import java.util.LinkedList;
import java.util.Random;

/**
 * 
 * 
 * @author Hugo Nørholm
 *
 */
public class Ludo {

    public static final int RED=0, BLUE=1, YELLOW=2, GREEN=3;
    
    public static LinkedList<DiceListener> dListener = new LinkedList<DiceListener>();;
    public static LinkedList<PieceListener> pListener = new LinkedList<PieceListener>();;
    public static LinkedList<PlayerListener> playerListeners = new LinkedList<PlayerListener>();;

    private String[] players = new String[4];
    private int [][] pieces = new int[][] {{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0}};
    private int playersTurn = 0;
    private int consecutiveTurn = 0;
    public String status = "Created";
	//private GameBoardController game;

    public Ludo(){

    }

    /**
     * Creates new game with up to 4 players 
     * 
     * @param player1 Red player
     * @param player2 Blue player
     * @param player3 Yellow player
     * @param player4 Green player
     * @throws NotEnoughPlayersException if only one player
     */
    public Ludo(String player1, String player2, String player3, String player4) throws NotEnoughPlayersException{
    	int i =0;
    	if(player1 != null) {
    		players[0] = player1;
    		i++;
    	}
    	if(player2 != null) {
    		players[1] = player2;
    		i++;
    	}
    	if(player3 != null) {
    		players[2] = player3;
    		i++;
    	}
    	if(player4 != null) {
    		players[3] = player4;
    		i++;
    	}
    	if(i < 2) {
    		throw new NotEnoughPlayersException();
    	} else {
			status = "Initiated";
		}

    }

    /**
     * counts players
     * 
     * @return  nr of players
     */
    public int nrOfPlayers(){
		for(int i=0;i<4;i++){
		    if(players[i] == null){;
		        return i;
            }
        }
		return 4;
	}

    /**
     * get a player name
     * 
     * @param player playrnr
     * @return	player name
     */
    public String getPlayerName(int player){
		return players[player];
		}

	/**
	 * Add a player to the game
	 * 
	 * @param string player name
	 * @throws NoRoomForMorePlayersException If there are already 4 players
	 */
	public void addPlayer(String string)throws NoRoomForMorePlayersException  {
		for(int i=0;i<4;i++){
		    if(players[i] == null){
		        players[i]= string;
		        if(status=="Created") {
		        	status = "Initiated";
		        }
		        return;
            }
        }
        throw new NoRoomForMorePlayersException();
	}

	/**
	 * Remove a player from the game if player removed is current player go next player
	 * 
	 * @param string player to remove
	 */
	public void removePlayer(String string) {
		for(int i=0;i<4;i++){
		    if(players[i] == string){
		        for(PlayerListener p: playerListeners){
		        	PlayerEvent pe = new PlayerEvent(this, i, PlayerEvent.LEFTGAME);
		            p.playerStateChanged(pe);
		        }
		        if(i==playersTurn) {
		        	nextPlayer();
		        }
		        players[i] = "Inactive: " + string;
            }
        }
	}

	/**
	 * Count active players
	 * 
	 * @return nrOactiveplayers
	 */
	public int activePlayers() {
		int j = 0;
		for(int i=0;i<4;i++){
		    if(!players[i].startsWith("Inactive")){;
		        j++;
            }
        }
		return j;
	}

	/**
	 * position of a players piece
	 * 
	 * @param player	
	 * @param piece
	 * @return	position
	 */
	public int getPosition(int player, int piece) {
		return pieces[player][piece];
	}

	/**
	 * whose turn is it
	 * 
	 * @return active player
	 */
	public int activePlayer() {
		return playersTurn;
	}
	
	/**
	 * Moves playersTurn to next active player
	 * 
	 */
	public void nextPlayer() {
        for(PlayerListener p: playerListeners){
        	PlayerEvent pe = new PlayerEvent(this, playersTurn, PlayerEvent.WAITING);
            p.playerStateChanged(pe);
        }
		consecutiveTurn = 0;
		do {
			if(playersTurn+1 == nrOfPlayers()) {
				playersTurn = 0;
			}
			else {
				playersTurn++;
			}
		}while(players[playersTurn].startsWith("Inactive"));
        for(PlayerListener p: playerListeners){
        	PlayerEvent pe = new PlayerEvent(this, playersTurn, PlayerEvent.PLAYING);
            p.playerStateChanged(pe);
        }
	}

	/**
	 * Throw set dice used for tests mostly
	 * 
	 * @param i dice thrown
	 * @return dice result
	 */
	public int throwDice(int i) {
		boolean blocked[] = new boolean[4];
		
		if(status=="Initiated") {
        	status = "Started";
        }
		
        for(DiceListener d: dListener){
        	DiceEvent de = new DiceEvent(this, playersTurn, i);
            d.diceThrown(de);
        }
        
        for(int j = 0; j<4;j++) {
        	blocked[j]=blocked(playersTurn,j,i);
        }


		if(pieces[playersTurn][0]==0 && pieces[playersTurn][1]==0 && pieces[playersTurn][2]==0 && pieces[playersTurn][3]==0) {	
			if (i < 6) {
				if(consecutiveTurn < 2) {
				consecutiveTurn++;
				}
				else {

					nextPlayer();
				}
			}
		}
		else if (i== 6)
			if(consecutiveTurn < 2) {
			consecutiveTurn++;
			}
			else {
				nextPlayer();
			}
		
		
		else if(blocked[0] && blocked[1] && blocked[2] && blocked[3]) {
			nextPlayer();
		}
		else if(((pieces[playersTurn][0]+i>59 && pieces[playersTurn][0]<59)
				|| (pieces[playersTurn][1]+i>59 && pieces[playersTurn][1]<59)
				|| (pieces[playersTurn][2]+i>59 && pieces[playersTurn][2]<59)
				|| (pieces[playersTurn][3]+i>59 && pieces[playersTurn][0]<59))
				&& i !=6) {
			nextPlayer();
		}
		return i;
		}

	
	/**
	 * Throws dice checks and calls nextplayer if currentplayer cannot move with dice thrown
	 *
	 * @return dice thrown
	 */
	public int throwDice() {
		int i = new Random().nextInt(6)+1;

		return throwDice(i);
	}


	/**
	 * moves one players piece from i to j does not check if blocked by tower 
	 * 
	 * @param player who is moving
	 * @param i from
	 * @param j to
	 * @return move success
	 */
	//TODO make return false if blocked by tower
	public boolean movePiece(int player, int i, int j) {
		int die;
		if(i == 0 && j==1) {
			die=6;
		}
		else {
			die = j-1;
		}
		for(int k = 0; k <4; k++) {
			if(!blocked(player,k,die)) {
				if (pieces[player][k]==i) {
					pieces[player][k]=j;
		        	for(PieceListener p: pListener){
		        		PieceEvent pe = new PieceEvent(this, player, k,i,j);
		            	p.pieceMoved(pe);
		        }
					hitOrMiss(player,j);
					if (pieces[player][0]==59 && pieces[player][1]==59 && pieces[player][2]==59 && pieces[player][3]==59) {
						status="Finished";
			        	for(PlayerListener p: playerListeners){
			        	PlayerEvent pe = new PlayerEvent(this, player, PlayerEvent.WON);
			        	p.playerStateChanged(pe);
			        	}
					}
					if(j-i!=6 || j==1) {
					nextPlayer();
					}
					//game.updateBoard(player, piece, globalPosition);	// Call to update board with changes.
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * 
	 * @return game status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * finds player with all pieces in base
	 * @return winning player
	 */
	public int getWinner() {
		for(int i = 0; i <4;i++) {
			if (pieces[i][0]==59 && pieces[i][1]==59 && pieces[i][2]==59 && pieces[i][3]==59) {
				return i;
			}
		}
		return 0;
	}

	/**
	 * converts player board pos to universal board pos
	 * 
	 * @param player player
	 * @param pos	position relative to player home
	 * @return universal pos
	 */
	public int userGridToLudoBoardGrid(int player, int pos) {
		int absPosition = 0;
		if(pos==0) {
			return player * 4;
		}
		else if (pos<54 && pos>0) {
			if (player==Ludo.RED) {
					absPosition = 15+pos;
				}
			else if (player==Ludo.BLUE) {
				absPosition = 28+pos;
			}
			else if (player==Ludo.YELLOW) {
				absPosition = 41+pos;
			}
			else if (player==Ludo.GREEN) {
				absPosition = 54+pos;
			}
			if(absPosition>67) {
				absPosition = absPosition - 67 + 15;
			}
			return absPosition;
		}
		else if (pos>53) {
			return pos + 14 + (player*6);
		}
		return 0;
	}
	
	/**
	 * checks if pos was occupied and knocks piece back to start if it was
	 * @param player player who moved
	 * @param pos position moved to
	 */
	public void hitOrMiss(int player, int pos) {
		for (int i=0;i<4;i++) {
			if(i!=player) {
			for (int j=0;j<4;j++) {
				if(userGridToLudoBoardGrid(i,pieces[i][j]) == userGridToLudoBoardGrid(player,pos)) {
			        for(PieceListener p: pListener){
			        	PieceEvent pe = new PieceEvent(this, i, j,pieces[i][j],0);
			            p.pieceMoved(pe);
			        }
					pieces[i][j]=0;
				}
			}	
			}
		}	
	}
	
	/**
	 * checks if a piece is blocked by a tower
	 * 
	 * @param player 
	 * @param die dice thrown by player
	 * @return true all moves blocked 
	 */
	public boolean blocked(int player, int mypiece, int die) {
		for (int enemy=0;enemy<4;enemy++) {
			if (enemy!=player) {
				for (int piece = 0; piece<3;piece++) {
					if(pieces[enemy][piece]!=0 && pieces[enemy][piece]<54) {
						for(int piece2 = piece+1; piece2<4; piece2++) {
							if(pieces[enemy][piece]==pieces[enemy][piece2]) {

								if(pieces[player][mypiece]==0) {
									if(userGridToLudoBoardGrid(player,1) == userGridToLudoBoardGrid(enemy,pieces[enemy][piece]) || die <6) {
										return true;
									}
								}
								else if(pieces[player][mypiece]<54){
									for(int i = 1; i<die+1;i ++) {	
										if(userGridToLudoBoardGrid(player,pieces[player][mypiece]+i) == userGridToLudoBoardGrid(enemy,pieces[enemy][piece])) {
											return true;
										}
									}
								}
							
							}
						}
					}
				}
			}
		}
		return false;
	}

	/**
	 * adds listener
	 * 
	 * @param diceListener
	 */
	public void addDiceListener(DiceListener diceListener) {
		dListener.add(diceListener);
		
	}

	/**
	 * adds listener
	 * 
	 * @param pieceListener
	 */
	public void addPieceListener(PieceListener pieceListener) {
		pListener.add(pieceListener);
		
	}

	/**
	 * adds listener
	 * 
	 * @param playerListener
	 */
	public void addPlayerListener(PlayerListener playerListener) {
		playerListeners.add(playerListener);
		
	}

}
