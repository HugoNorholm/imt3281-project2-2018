package no.ntnu.imt3281.ludo.logic;

public class DiceEvent{
	private Ludo game;
	private int player;
	private int die;
	
	public DiceEvent(Ludo ludo, int i, int j) {
		game=ludo;
		player = i;
		die = j;
	}
	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
	            return true;
	        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }
        DiceEvent temp = (DiceEvent) obj;
		if(temp.game != this.game) {
			return false;
		}
		if(temp.player != this.player) {
			return false;
		}
		if(temp.die != this.die) {
			return false;
		}
		return true;
	}

	/**
	 * Override of library func to avoid deep conflicts.
	 * @return 0
	 */
  	@Override
  	public int hashCode() {
    	return 0;
  	}
}
