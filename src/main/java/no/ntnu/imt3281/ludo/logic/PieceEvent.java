package no.ntnu.imt3281.ludo.logic;

public class PieceEvent {
	private Ludo game;
	private int player;
	private int piece;
	private int start;
	private int destination;
	
	public PieceEvent(Ludo ludo, int i, int j, int k, int l) {
		 game=ludo;
		 player=i;
		 piece=j;
		 start=k;
		 destination=l;		 
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
	            return true;
	        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }
        PieceEvent temp = (PieceEvent) obj;
		if(temp.game != this.game) {
			return false;
		}
		if(temp.player != this.player) {
			return false;
		}
		if(temp.piece != this.piece) {
			return false;
		}
		if(temp.start != this.start) {
			return false;
		}
		if(temp.destination != this.destination) {
			return false;
		}
		return true;
	}

	/**
	 * Override of library func to avoid deep conflicts.
	 * @return 0
	 */
	@Override
	public int hashCode() {
		return 0;
	}

}
