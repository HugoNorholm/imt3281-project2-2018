package no.ntnu.imt3281.ludo.logic;

/**
 * No matching piece could be found
 */
public class NoMatchingPieceException extends Exception {
    private String message;

    public NoMatchingPieceException(String _string) {
        message = _string;
    }
}
