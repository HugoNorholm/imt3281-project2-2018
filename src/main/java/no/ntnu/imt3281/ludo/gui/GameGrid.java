package no.ntnu.imt3281.ludo.gui;

import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.layout.GridPane;
import javafx.scene.shape.Circle;

/**
 * Object for indexing coordinates of a GridPane node within a section and storing data about the node.
 *
 */
public class GameGrid {
    private GridPane section;
    private int x;
    private int y;
    public boolean hasPiece;
    private int pieceColour;
    private int pieceNumber;

    public GameGrid() {
        /* Empty constructor */
    }

    /**
     * Assigns GridPane and coordinates within respective GridPane.
     *
     * @param sectionParam  GridPane section, eg. North, South, Goal, etc
     * @param xParam
     * @param yParam
     */
    public GameGrid(GridPane sectionParam, int xParam, int yParam) {
        section = sectionParam;
        x = xParam;
        y = yParam;
        hasPiece = false;
        pieceColour = -1;
        pieceNumber = -1;
    }

    /**
     *
     * @return  GridPane fx:id.
     */
    public GridPane getSection(){ return section; }

    /**
     * Takes a javafx Circle, player number, and number of moved piece, adds the piece to the grid,
     * then stores the player and piece numbers and identifies as having a piece for identifying later.
     *
     * @param piece     Javafx Circle of desired colour.
     * @param player    Number of the player.
     * @param number    Number of the piece.
     */
    public void addPiece(Circle piece, int player, int number) {
        try {
            if (piece == null ||  player < 0 || player > 3|| number < 0 || number > 3) {
                throw new Exception("Invalid data entered. Expected fx:Circle, int 0-3, int 0-3");
            }
            section.add(piece, x, y);

            hasPiece=true;
            pieceColour=player;
            pieceNumber=number;
        } catch (Exception e) {
            System.out.println("Error:" + e.getMessage());
        }
    }

    /**
     * Check if piece in position matches parameter piece details.
     *
     * @param player    Player id
     * @param number    Respective piece number
     * @return True if player and piece id matches.
     */
    public boolean pieceMatch(int player, int number) {
        return (player == pieceColour && number == pieceNumber);
    }

    /**
     * Goes through grid to find piece node, then removes it if finds the right one.
     * I blame this horror on the Javafx api for not making it possible to access nodes by coordinates
     *
     * @return true if found node and removed it.
     */
    public boolean removePiece() {
        ObservableList<Node> children = section.getChildren();

        for (Node node : children) {
            if (GridPane.getRowIndex(node) == x && GridPane.getColumnIndex(node) == y) {
                section.getChildren().remove(node);
                hasPiece = false;
                pieceColour = -1;
                pieceNumber = -1;
                return true;
            }
        }
        return false;
    }

    /**
     * @param _section  fx:id of GridPane
     * @param _x    x coordinate
     * @param _y    y coordinate
     * @return      true if coordinates match
     */
    public boolean matchXY(GridPane _section, int _x, int _y) {
        boolean res;
        try {
            res = ( section == _section && x == _x && y == _y);
        } catch (Exception e) {
            res = false;
            System.out.println("MatchXY error: " + e.getMessage());
        }

        return res;
    }
}
