package no.ntnu.imt3281.ludo.gui;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.reflect.InvocationTargetException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class LudoController {

    private static final Logger LOGGER = Logger.getLogger( LudoController.class.getName() );
	ResourceBundle output = ResourceBundle.getBundle("no.ntnu.imt3281.I18N.i18n");
	
	String loggedInUser;
	
	public final static int[] port = new int[] {9010,9020,9030,9040};
	private final static ConcurrentHashMap<String, Connection> chatConns = new ConcurrentHashMap<>();
	static ExecutorService executor = Executors.newCachedThreadPool();
	
	private boolean connected = false;
	String pattern = "^[a-zA-Z0-9]+$";
	private Connection connection;
	/*
	// auto login disabled for testing 
    @FXML
    public void initialize() {

		InputStream input = null;
		Properties prop = new Properties();
    	try {
    		input = new FileInputStream("config.properties");
    		// load a properties file
    		prop.load(input);

    		// get the property value and print it out
    		String user = prop.getProperty("name");
    		String pass = prop.getProperty("pass");
    		login(user,pass);
			connectbut.setDisable(true);
			signup.setDisable(true);
			disconnectbut.setDisable(false);

			loggedInUser = user;
			joindRoom("MASTER");

    	} catch (IOException ex) {
    		ex.printStackTrace();
    	} finally {
    		if (input != null) {
    			try {
    				input.close();
    			} catch (IOException e) {
    				e.printStackTrace();
    			}
    		}
    	}
    }
    */

    @FXML
    private MenuItem random;
    
    @FXML
    private MenuItem connectbut;
    
    @FXML
    private MenuItem signup;
    
    @FXML
    private MenuItem disconnectbut;

    @FXML
    private TabPane tabbedPane;

    @FXML
    public void joinRandomGame(ActionEvent e) throws IOException {
    	FXMLLoader loader = new FXMLLoader(getClass().getResource("GameBoard.fxml"));
    	loader.setResources(ResourceBundle.getBundle("no.ntnu.imt3281.I18N.i18n"));

	//	GameBoardController controller = loader.getController();
		// Use controller to set up communication for this game.
		// Note, a new game tab would be created due to some communication from the server
		// This is here purely to illustrate how a layout is loaded and added to a tab pane.

    	try {
            Parent gameBoard = loader.load();
        	Tab tab = new Tab("Game");
    		tab.setContent(gameBoard);
        	tabbedPane.getTabs().add(tab);
    	} catch (IOException e1) {
			// TODO Auto-generated catch block
			//e1.printStackTrace();
			LOGGER.log(Level.WARNING, e1.toString());
		}

    }

    /**
     * Asks what room you want to join and lets you join it
     * @param e
     */
    @FXML
    public void joinRoom(ActionEvent e) {
        Stage newStage = new Stage();
        VBox comp = new VBox();
        TextField chatname = new TextField(output.getString("roomname"));
        Button submit = new Button(output.getString("submit"));
        TextArea response = new TextArea();
        submit.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                String name = chatname.getText();
                if(name.matches(pattern)) { //Todo check if room exists?
                    joindRoom(name);
                    chatname.setManaged(false);
                    submit.setManaged(false);
                }

                else {response.setText(output.getString("roomname")+" "+output.getString("alpha"));}
            }
        });
        comp.getChildren().add(chatname);
        comp.getChildren().add(submit);
        comp.getChildren().add(response);


        Scene stageScene = new Scene(comp, 300, 150);
        newStage.setScene(stageScene);
        newStage.setTitle(output.getString("joinroom"));
        newStage.show();

    }

    /**
     * Function lists all active rooms
     * @param e		fx:ActionEvent
     */
    @FXML
    public void roomList(ActionEvent e) {

    		try {
				connection.send("RL");
				String tmp = connection.input.readLine();
				if (tmp == null) {
				    throw new IOException("Input can not be null");
                }
				String roomlist[] = tmp.split(":");
				Stage newStage = new Stage();
				TextArea chatlist = new TextArea();
				for (String s: roomlist) {
			        chatlist.appendText(s+"\n");
			    }

				VBox comp = new VBox();
				comp.getChildren().add(chatlist);
		    	Scene stageScene = new Scene(comp, 300, 150);
		    	newStage.setScene(stageScene);
		    	newStage.setTitle(output.getString("roomlist"));
		    	newStage.show();

			} catch (IOException ex) {
                // System.out.println("Error: " + ex.getMessage());
                LOGGER.log(Level.WARNING, ex.toString(), ex);
			}


    }


    /**
     * Function for allowing to create and automatically join a new chatroom.
     * @param e		fx:ActionEvent
     */
	@FXML
    public void createRoom(ActionEvent e) {
    	Stage newStage = new Stage();
    	VBox comp = new VBox();
    	TextField chatname = new TextField(output.getString("roomname"));
    	Button submit = new Button(output.getString("submit"));
    	TextArea response = new TextArea();
    	submit.setOnAction(new EventHandler<ActionEvent>() {
    	     public void handle(ActionEvent e) {
    	    	String name = chatname.getText();
    	    	if(name.matches(pattern)) {
    	    			
    					
						try {
							String tmp;
							connection.send("C:"+name);
							tmp = connection.input.readLine();
                            if (tmp == null) {
                                throw new IOException("Input can not be null");
                            }
	    					if (tmp.startsWith("success")) {
	    						System.out.println("succ");
	    						chatname.setManaged(false);
	    						submit.setManaged(false);	
	    						response.setText(tmp);
	    						joindRoom(name);
	    						
	    					} else if (tmp.startsWith("exists")) {
	    						System.out.println("fail");
	    						response.setText(output.getString("chat")+": "+name+output.getString("exists")+".");
	    					}
						} catch (IOException el) {
                            LOGGER.log(Level.WARNING, el.toString(), el);
						}
    					


    				
    	    	}
    	     
    	    	else {response.setText("Room name must be alphanumeric");}
    	    }   	    
    	});
    	comp.getChildren().add(chatname);
    	comp.getChildren().add(submit);
    	comp.getChildren().add(response);
    	

    	Scene stageScene = new Scene(comp, 300, 150);
    	newStage.setScene(stageScene);
    	newStage.setTitle(output.getString("createroom"));
    	newStage.show();
    	
    }
    
    /**
     * Join a chat room will break on nonexistant rooms
     * @param name name of room to join
     */
    public void joindRoom(String name) {  	
    	try {
    		Connection con = new Connection(port[2]);
			con.send(loggedInUser);
			con.send(name);

			AnchorPane chat = new AnchorPane();
			VBox box = new VBox();
			box.setPrefSize(300, 800);
			TextArea text = new TextArea();
			text.setPrefHeight(600);
			HBox mgBox = new HBox();
			
	    	TextField msg = new TextField();
	    	msg.setPrefWidth(230);
	    	Button sendmsg = new Button(output.getString("ludogameboard.saybutton"));
	    	sendmsg.setPrefWidth(70);
	    	
	    	chat.getChildren().add(box);
	    	box.getChildren().add(text);
	    	box.getChildren().add(mgBox);
	    	
	    	mgBox.getChildren().add(msg);
	    	mgBox.getChildren().add(sendmsg);
		
        	Tab tab = new Tab(name);
    		tab.setContent(chat);
        	tabbedPane.getTabs().add(tab);
        	
            sendmsg.setOnAction(new EventHandler<ActionEvent>() {

    			@Override
    			public void handle(ActionEvent arg0) {
    				try {
    					con.send(msg.getText());
    				} catch (IOException e) {
                        LOGGER.log(Level.WARNING, e.toString(), e);
    				}
    			}

       	});
        	
        	chatConns.put(name,con);
        	executor.execute(()->chatHandlerThread(con,text));
        	
        	
    	} catch (IOException el) {
            LOGGER.log(Level.WARNING, el.toString(), el);
		}
    }
   /**
    * Handles listening for incoming chat messages
    * @param con chat connection
    * @param text area chat is being written to
    */
    private void chatHandlerThread(Connection con, TextArea text) {
		while (con.active) {
			
			String incmsg;
			incmsg = con.read();
			if(incmsg!=null) {
			text.appendText(incmsg+"\n");
			}
		}
	}

	@FXML
    public void disconnect(ActionEvent e) {
    	connection.close();
    }

    /**
     * Login fuction
     * @param e
     */
    @FXML
    public void connect(ActionEvent e) {
    	Stage newStage = new Stage();
    	VBox comp = new VBox();
    	TextField username = new TextField(output.getString("username"));
    	TextField pass = new TextField(output.getString("pass"));
    	Button submit = new Button(output.getString("submit"));
    	TextArea response = new TextArea();
    	submit.setOnAction(new EventHandler<ActionEvent>() {
    	     public void handle(ActionEvent e) {
    	    	String name = username.getText();
    	    	String password = username.getText();
    	    	
    	    	if(name.matches(pattern) && password.matches(pattern)) {
    	    	if (connected) {				// Currently connected, disconnect from server
    				connected = false;
    				connection.close();			// This will send a message to the server notifying the server about this client leaving
    	    	} else {
    				try {
    					login(name,password);
    					String tmp = connection.input.readLine();
    					// System.out.println(tmp);
                        if (tmp == null) {
                            throw new IOException("Input can not be null");
                        }
    					if (tmp.equals("success")) {
    						username.setManaged(false);
    						pass.setManaged(false);
    						submit.setManaged(false);
    						connectbut.setDisable(true);
    						signup.setDisable(true);
    						disconnectbut.setDisable(false);
    						
    						
    						response.setText(output.getString("loggedin")+": "+name);
    						loggedInUser = name;
    						joindRoom("MASTER");
    						
    					} else if (tmp.equals("wrong")) {
    						response.setText(output.getString("wpass"));
    					}
    					else if (tmp.equals("failure")) {
						    response.setText(output.getString("wuser"));
						}

    				} catch (UnknownHostException a) {
    					// Ignored, means no connection (should have informed the user.)
                        LOGGER.log(Level.WARNING, a.toString(), a);
    				}
    				catch (IOException a) {
    					// Ignored, means no connection (should have informed the user.)
                        LOGGER.log(Level.WARNING, a.toString(), a);
    				}
    	    	}
    	     }
    	    	else {response.setText("Username and password must be alphanumeric");}
    	    }   	    
    	});
    	comp.getChildren().add(username);
    	comp.getChildren().add(pass);
    	comp.getChildren().add(submit);
    	comp.getChildren().add(response);
    	

    	Scene stageScene = new Scene(comp, 300, 150);
    	newStage.setScene(stageScene);
    	newStage.setTitle("Login");
    	newStage.show();
    }

    /**
     * Signup function
     * @param e
     */
    @FXML
    public void createUser(ActionEvent e) {     	
    	Stage newStage = new Stage();
    	VBox comp = new VBox();
    	TextField username = new TextField(output.getString("username"));
    	TextField pass = new TextField(output.getString("pass"));
    	Button submit = new Button(output.getString("submit"));
    	TextArea response = new TextArea();
    	submit.setOnAction(new EventHandler<ActionEvent>() {
    	     public void handle(ActionEvent e) {
    	    	String name = username.getText();
    	    	String password = username.getText();
    	    	
    	    	if(name.matches(pattern) && password.matches(pattern)) {
    	    	if (connected) {				// Currently connected, disconnect from server
    				connected = false;
    				connection.close();			// This will send a message to the server notifying the server about this client leaving
    	    	} else {
    				try {
    					connection = new Connection(port[0]);			// Connect to the server
    					connected = true;
    					connection.send(name); 
    					connection.send(password); // Send username
    					String tmp = connection.input.readLine();
                        if (tmp == null) {
                            throw new IOException("Input can not be null");
                        }

    					if (tmp.endsWith("created!")) {
    						username.setManaged(false);
    						pass.setManaged(false);
    						submit.setManaged(false);
    	    				connected = false;
    	    				connection.close();	
    	    				//store user info in prop file
    	    				Properties prop = new Properties();
    	    		        prop.setProperty("name", name);
    	    		        prop.setProperty("pass", password);
    	    				FileOutputStream fileOut = new FileOutputStream("config.properties");
							prop.store(fileOut, "User info");
    	    				
    						response.setText(name + output.getString("created"));
    						
    					} else if (tmp.endsWith("exists")) {
    						response.setText(name + output.getString("uexist"));
    					}

    				} catch (UnknownHostException a) {
    					// Ignored, means no connection (should have informed the user.)
                        LOGGER.log(Level.WARNING, a.toString(), a);
    				}
    				catch (IOException a) {
    					// Ignored, means no connection (should have informed the user.)
                        LOGGER.log(Level.WARNING, a.toString(), a);
    				}
    	    	}
    	     }
    	    	else {response.setText("Username and password must be alphanumeric");}
    	    }   	    
    	});
    	comp.getChildren().add(username);
    	comp.getChildren().add(pass);
    	comp.getChildren().add(submit);
    	comp.getChildren().add(response);
    	

    	Scene stageScene = new Scene(comp, 300, 150);
    	newStage.setScene(stageScene);
    	newStage.setTitle("Create User");
    	newStage.show();
    	
    }
    
    public void login(String name, String pass) throws IOException {
		connection = new Connection(port[1]);			// Connect to the server
		connected = true;
		connection.send(name); 
		connection.send(pass); // Send username
    }	
    	
    
    static class Connection {
    	private final Socket socket;
    	private final BufferedReader input;
    	private final BufferedWriter output;
    	private boolean active = true;
    	
    	public Connection(int port) throws IOException {
    		socket = new Socket("localhost", port);
    		input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
    		output = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
    	}
    	
    	public void send(String text) throws IOException {
    		output.write(text);
    		output.newLine();
    		output.flush();
    	}
    	
    	public String read() {
    		try {
				if (input.ready()) {
					return input.readLine();
				} else {
					return null;
				}
			} catch (IOException e) {
				active = false;	// If unable to read (IO error) mark as inactive, this will remove the client from the server
				return null;
			}
    	}
    	
    	public void close() {
    		try {//send goodbye
				output.close();
	    		input.close();
	    		socket.close();
			} catch (IOException e) {
				// Nothing to do, the connection is closed
			}
    	}
}
}

