package no.ntnu.imt3281.ludo.gui;


import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.event.ActionEvent;

/**
 * Sample Skeleton for 'GameBoard.fxml' Controller Class
 */

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.shape.Circle;
import no.ntnu.imt3281.ludo.logic.Ludo;
import no.ntnu.imt3281.ludo.logic.NotEnoughPlayersException;
import no.ntnu.imt3281.ludo.logic.OutOfBoundsException;
import no.ntnu.imt3281.ludo.logic.NoMatchingPieceException;

/**
 * Controller class for everything relating to the gameboard,
 * eg. sections, visual piece representation, fx:ids
 */
public class GameBoardController {

    @FXML
    private GridPane north;

    @FXML
    private GridPane east;

    @FXML
    private GridPane west;

    @FXML
    private GridPane south;

    @FXML
    private GridPane redHome;

    @FXML
    private GridPane blueHome;

    @FXML
    private GridPane yellowHome;

    @FXML
    private GridPane greenHome;

    @FXML
    private GridPane goal;

    @FXML
    private Label player1Name;

    @FXML
    private ImageView player1Active;

    @FXML
    private Label player2Name;

    @FXML
    private ImageView player2Active;

    @FXML
    private Label player3Name;

    @FXML
    private ImageView player3Active;

    @FXML
    private Label player4Name;

    @FXML
    private ImageView player4Active;

    @FXML
    private ImageView diceThrown;

    @FXML
    private Button throwTheDice;

    @FXML
    private TextArea chatArea;

    @FXML
    private TextField textToSay;

    @FXML
    private Button sendTextButton;

    /**
     * Stores the visuals for all pieces in play.
     */
    private Circle[][] pieces = {
            {new Circle(), new Circle(), new Circle(), new Circle()},
            {new Circle(), new Circle(), new Circle(), new Circle()},
            {new Circle(), new Circle(), new Circle(), new Circle()},
            {new Circle(), new Circle(), new Circle(), new Circle()}
    };

    private GameGrid[] grid = new GameGrid[93];

    @FXML
    public void initialize() {
        try {
            indexGrid();
            player1Active.setVisible(false);
            player2Active.setVisible(false);
            player3Active.setVisible(false);
            player4Active.setVisible(false);

            for (int i=0; i < 4; i++) {
                for ( int j = (i*4); j == (i*4)+4; j++) {
                    // Initialise player pieces:
                    switch (i) {
                        case (0):
                            for (int y = 0; y < 4; y++) {
                                pieces[i][y] = new Circle(22.f, Color.RED);
                                pieces[i][y].setStroke(Color.BLACK);
                            } break;
                        case (1):
                            for (int y = 0; y < 4; y++) {
                                pieces[i][y] = new Circle(22.f, Color.BLUE);
                                pieces[i][y].setStroke(Color.BLACK);
                            } break;
                        case (2):
                            for (int y = 0; y < 4; y++) {
                                pieces[i][y] = new Circle(22.f, Color.YELLOW);
                                pieces[i][y].setStroke(Color.BLACK);
                            } break;
                        case (3):
                            for (int y = 0; y < 4; y++) {
                                pieces[i][y] = new Circle(22.f, Color.GREEN);
                                pieces[i][y].setStroke(Color.BLACK);
                            } break;
                    }

                    // Add the piece to home zone pos relevant to which piece it is.
                    grid[j].addPiece(pieces[i][j%4], i, j%4);
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    @FXML
    public void throwDice(ActionEvent e) throws NotEnoughPlayersException {
    	Ludo game = new Ludo("player1","player2",null,null);
    	String imgsrc = null;
    	int dice = game.throwDice();
    	switch (dice) {
    	case 1:
    		imgsrc = "../../../../../images/dice1.png";
    		break;
    	case 2:
    		imgsrc = "../../../../../images/dice2.png";
    		break;
    	case 3:
    		imgsrc = "../../../../../images/dice3.png";
    		break;
    	case 4:
    		imgsrc = "../../../../../images/dice4.png";
    		break;
    	case 5:
    		imgsrc = "../../../../../images/dice5.png";
    		break;
    	case 6:
    		imgsrc = "../../../../../images/dice6.png";
    		break;
        default:
        	imgsrc = "../../../../../images/dice1.png";
        	break;
    	}

		Image image = new Image(getClass().getResourceAsStream(imgsrc));
		diceThrown.setImage(image);
    }

    @FXML
    public void postMessage(ActionEvent event) {
        try {

            String tempChat, tempMsg;
            tempChat = chatArea.textProperty().getValueSafe();
            tempMsg = textToSay.textProperty().getValueSafe();

            tempChat += tempMsg+"\n";

            chatArea.textProperty().setValue(tempChat);
            textToSay.textProperty().setValue(null);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    @FXML
    void selectPiece(MouseEvent event) {

        int x, y, posX, posY;
        GridPane temp = new GridPane();
        temp.setHgap(-1); temp.setVgap(-1);

        x = (int)event.getSceneX() / 48;
        y = (int)event.getSceneY() / 48;

        posX = (int)event.getX() / 48;
        posY = (int)event.getY() / 48;

        // Assigns pressed section to temp
        if ( 1 <= y && y <= 6 ) {
            temp = north;
        } else if ( 10 <= y && y <= 15 ) {
            temp = south;
        } else if ( 0 <= x && x <= 5 ) {
            temp = west;
        } else if ( 9 <= x && x <= 14 ) {
            temp = east;
        }

        try {
            if (temp.getHgap() != -1 && temp.getVgap() != -1) {
                int i = 0;
                while (grid[i] != null && !grid[i].matchXY(temp, posX, posY) && i < 93) {
                    i++;
                }
                if (grid[i] != null && !grid[i].matchXY(temp, posX, posY)) {
                    throw new NoMatchingPieceException("No piece in this square");
                } else {
                    // Temp to test input
                    grid[i].addPiece(pieces[0][0], 0, 0);
                }
            } else {
                throw new OutOfBoundsException("Out of bounds click");
            }
        }
        catch (OutOfBoundsException | NoMatchingPieceException ex ) {
            System.out.println("checking grid: " + ex.getMessage());
            return;
        }


        //TODO: communicate with logic that piece was chosen.
        //if (grid[].hasPiece) { }

    }

    /**
     * Add active players' pieces to respective home zones
     *
     * @param numOfPlayers number of players
     */
    public void initPlayers(int numOfPlayers) throws NullPointerException {

        for (int i=0; i < numOfPlayers; i++) {
            for ( int j = (i*4); j == (i*4)+4; j++) {
                // Add the piece to home zone pos relevant to which piece it is.
                grid[j].addPiece(pieces[i][j%4], i, j%4);
            }
        }
    }


    /**
     * Traverses through the board and associates grid-coordinates to their respective absolute position,
     *
     * Very dirty solution, but only run once as board initialises, afterwards the ArrayList will be used
     * to access through global absolute positions.
     */
    private void indexGrid() {
        setupHomeZones();
        setupNorth();
        setupEast();
        setupSouth();
        setupWest();
    }

    /**
     * Set up home sections node to these coordinates:
     *
     */
    private void setupHomeZones() {
        GridPane[] homeZones = {redHome, blueHome, yellowHome, greenHome};

        for (int i = 0; i < 16; i+=4) {
            grid[i] = new GameGrid(homeZones[i / 4], 2, 1);
            grid[i+1] = new GameGrid(homeZones[i / 4], 3, 2);
            grid[i+2] = new GameGrid(homeZones[i / 4], 1, 2);
            grid[i+3] = new GameGrid(homeZones[i / 4], 2, 4);
        }
    }

    /**
     * Index North section and goal node to these coordinates:
     * 16-20, 60-67, 68-73
     */
    private void setupNorth() {
        int i = 16;
        for (int y = 1; y < 6; y++) {
            grid[i] = new GameGrid(north, 2, y); i++;
        }

        i = 60;
        for (int y = 5; y>0; y--) {
            grid[i] = new GameGrid(north, 0, y); i++;
        }
        for (int x = 0; x < 3; x++) {
            grid[i] = new GameGrid(north, x, 0); i++;
        }
        for (int y = 1; y < 6; y++) {
            grid[i] = new GameGrid(north, 1, y); i++;
        }
        grid[i] = new GameGrid(goal, 1, 0);
    }

    /**
     * Index East section and goal node to these coordinates:
     * 21-33, 74-79
     */
    private void setupEast() {
        int i = 21;

        for (int x = 0; x<5; x++) {
            grid[i] = new GameGrid(east, x, 0); i++;
        }
        for (int y = 0; y<3; y++) {
            grid[i] = new GameGrid(east, 5, y); i++;
        }
        for (int x = 4; x >= 0; x--) {
            grid[i] = new GameGrid(east, x, 2); i++;
        }

        i = 74;
        for (int x = 4; x >= 0; x--) {
            grid[i] = new GameGrid(east, x, 1); i++;
        }
        grid[i] = new GameGrid(goal, 2, 1);
    }

    /**
     * Index South section and goal node to these coordinates:
     * 34-46, 80-85
     */
    private void setupSouth() {
        int i = 34;

        for (int y = 0; y<5; y++) {
            grid[i] = new GameGrid(south, 2, y); i++;
        }
        for (int x = 2; x>=0; x--) {
            grid[i] = new GameGrid(south, x, 5); i++;
        }
        for (int y = 4; y >= 0; y--) {
            grid[i] = new GameGrid(south, 0, y); i++;
        }

        i = 80;
        for (int y = 4; y >= 0; y--) {
            grid[i] = new GameGrid(south, 1, y); i++;
        }
        grid[i] = new GameGrid(goal, 1, 2);
    }

    /**
     * Index West section and goal node to these coordinates:
     * 47-59, 86-91
     */
    private void setupWest() {
        int i = 47;

        for (int x = 5; x > 0; x--) {
            grid[i] = new GameGrid(west, x, 2); i++;
        }
        for (int y = 2; y>=0; y--) {
            grid[i] = new GameGrid(west, 0, y); i++;
        }
        for (int x = 1; x <= 5; x++) {
            grid[i] = new GameGrid(west, x, 0); i++;
        }

        i = 86;
        for (int x = 1; x < 6; x++) {
            grid[i] = new GameGrid(west, x, 1); i++;
        }
        grid[i] = new GameGrid(goal, 0, 1);
    }



    /**
     * Updates gameboard by searching previous fields and removing piece if
     *
     * @param player    player number
     * @param piece     piece number
     * @param newPos    global position the piece is being moved to
     */
    public void updateGameboard(int player, int piece, int newPos) {

        // If new position is not the first square of respective player:
        if (newPos != 16 && player == 0 || newPos != 29 && player == 1 || newPos != 42 && player == 2 || newPos != 55 && player == 3) {

            int currentPos = newPos;
            boolean removed = false;
            do {
                if (grid[currentPos].hasPiece) {
                    if (grid[currentPos].pieceMatch(player, piece)) {
                        removed = grid[currentPos].removePiece();
                    }
                }
                currentPos--;
            } while (currentPos > newPos - 6 || currentPos > 15 && !removed);
        } else {
            // Remove correct piece from start zone if relevant:
            if (newPos == 16 && player == 0) grid[piece].removePiece();
            else if (newPos == 29 && player == 1) grid[4+piece].removePiece();
            else if (newPos == 42 && player == 2) grid[8+piece].removePiece();
            else if (newPos == 55 && player == 3) grid[12+piece].removePiece();
        }

        grid[newPos].addPiece(pieces[player][piece], player, piece);
    }

    /**
     * Takes player number and activates relevant active player flag in gui. Disables the flag of the previous player.
     *
     * @param player    Player id number
     */
    public void setActivePlayer(int player) {
        switch (player) {
            case (0): player4Active.setVisible(false); player1Active.setVisible(true); break;
            case (1): player1Active.setVisible(false); player2Active.setVisible(true); break;
            case (2): player2Active.setVisible(false); player3Active.setVisible(true); break;
            case (3): player3Active.setVisible(false); player4Active.setVisible(true); break;
        }
    }

}
