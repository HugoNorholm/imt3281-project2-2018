package no.ntnu.imt3281.ludo.gui;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

public class ChatController {
    @FXML
    private TextArea chatArea;

    @FXML
    private TextField textToSay;
    
    @FXML
    private Button sendTextButton;
	
}
